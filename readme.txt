BattleUI:

Aim -
Create a JRPG style battle screen with working interface and *simple* battle system.
Build to explore angularJS architecture and 'app' style interface design rather than to develop a working game.
Make UI pretty, but not designed to be mobile friendly.
Dropping .NET for this particular project. The focus is on angular, so going with node/express to keep the rest light.

Requirements -
Controllable by keyboard and mouse (iffy on this one, focus mouse)
Battles restart after win/loss (with random sprite changes to enemey composition) - narrow scope. Stick to this.
Animated health bar loss on hit (css?), splat boxes for enemies would also be nice.
Working inventory screen (from inventory command)
Working Results screen (animate some exp rewards)

Notes -
Art assets from FFIV (original version) available here - http://www.videogamesprites.net/FinalFantasy4/
run 'node server' from project dir with node console. In browser navigate to localhost:3000