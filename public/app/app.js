(function() {
  var app = angular.module('battle', []);

  var random = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /*
    BATTLE CONTROLLER
      main controller - orchestrates the battle
  */
  app.controller('battleController',['$scope', 'battleFactory', function($scope, battleFactory){

    $scope.characters = battleFactory.getCharacters();
    $scope.mobs = battleFactory.getMobs();
    $scope.statusMessage = battleFactory.getStatus();

    var rand = random(1, 10);
    $scope.background = 'assets/img/backgrounds/background' + rand + '.jpg';
  }]);

  /*
    BATTLE SERVICE
    for glueing it all together - just hacking at this really
  */
  app.factory('battleFactory', ['$timeout', function($timeout){

    //all the state values necessary for running a battle
    var characters = [
      {
        index: 0,
        name: "Edward",
        health: 20,
        maxHealth: 20,
        mana: 10,
        maxMana: 10,
        hasTurn: true,
        ko: false,
        graphic: 'assets/img/portraits/portrait1.png'
      },
      {
        index: 1,
        name: "Cid",
        health: 20,
        maxHealth: 20,
        mana: 10,
        maxMana: 10,
        hasTurn: false,
        ko: false,
        graphic: 'assets/img/portraits/portrait2.png'
      },
      {
        index: 2,
        name: "Cleo",
        health: 20,
        maxHealth: 20,
        mana: 10,
        maxMana: 10,
        hasTurn: false,
        ko: false,
        graphic: 'assets/img/portraits/portrait3.png'
      }
    ];
    var mobs = [
      {
        index: 0,
        name: 'Enemy-A',
        health: 20,
        maxHealth: 20,
        mana: 10,
        maxMana: 10,
        hasTurn: false,
        ko: false,
        graphic: 'assets/img/mobs/mob' + random(1,10) + '.png'
      },
      {
        index: 1,
        name: 'Enemy-B',
        health: 20,
        maxHealth: 20,
        mana: 10,
        maxMana: 10,
        hasTurn: false,
        ko: false,
        graphic: 'assets/img/mobs/mob' + random(1,10) + '.png'
      },
      {
        index: 2,
        name: 'Enemy-C',
        health: 20,
        maxHealth: 20,
        mana: 10,
        maxMana: 10,
        hasTurn: false,
        ko: false,
        graphic: 'assets/img/mobs/mob' + random(1,10) + '.png'
      }
    ];
    var actions = [
      {
        name: 'attack',
        mp: 0,
        invoke: function(i) {
          pendingAction = 'attack';
          waitingforMobTarget = true;
          status.message = 'select target';
        }
      },
      {
        name: 'defend',
        mp: 0,
        invoke: function(i) {
          status.message = characters[i].name + ' defends';
          nextTurn();
        }
      },
      {
        name: 'magic',
        mp: 0,
        actions: [
          {
            name: 'firebolt',
            mp: 5,
            invoke: function(i) {
              pendingAction = 'firebolt';
              waitingforMobTarget = true;
              status.message = 'select target';
            }
          },
          {
            name: 'lightning',
            mp: 5,
            invoke: function(i) {
              pendingAction = 'lightning';
              waitingforMobTarget = true;
              status.message = 'select target';
            }
          }
        ]
      },
      {
        name: 'item',
        mp: 0,
        invoke: function(i) {alert('item system not implemented');}
      }
    ];
    var status = { message : '...'};
    var pendingActor = characters[0];
    var pendingAction;
    var waitingforMobTarget=false;

    //clears status display, checks win/lose state and chooses whether mobs
    //or players act next (at rnd)
    var nextTurn = function() {

      $timeout(function() {
        /*have race condition with underlying angular watch digest not
        processing hasTurn if it's toggled too quickly.
        Put into seperate timeout to guarentee delay between value changes*/
        pendingActor.hasTurn = false;
      }, 1500);

      $timeout(function() {
        if (characters[0].ko && characters[1].ko && characters[2].ko) {
          status.message = 'YOU LOSE!!!!';
          return;
        }
        if (mobs[0].ko && mobs[1].ko && mobs[2].ko) {
          status.message = 'YOU WIN!!!!';
          return;
        }
        status.message='';
        if (random(1,2) == 1)
          nextPlayerMove();
        else
          dumbAi();
      }, 2000);
    };

    //chooses a player at rnd to act next
    var nextPlayerMove = function() {
      var i;
      do {
        i = random(0,2);
      } while (characters[i].ko);
      characters[i].hasTurn = true;
      pendingActor = characters[i];
      console.log(characters[i].name + "'s turn");;
    };

    //dumbest ai ever - rnd mob attacks rnd player for rnd damage
    var dumbAi = function() {
      var i;
      var j;
      do {
        i = random(0,2);
      } while (characters[i].ko);
      do {
        j = random(0,2);
      } while (mobs[j].ko);
      mobs[j].hasTurn = true;
      console.log(mobs[j].name + "'s turn");
      pendingActor = mobs[j];
      hostileAction(mobs[j], characters[i], 'attacks', random(1,6), 0);
      nextTurn();
    };

    //call back for when a mob has been clicked (ie selected as a target)
    var hostileActionHandler = function(i) {
      if (!waitingforMobTarget || mobs[i].health == 0)
        return;

      if (pendingAction == 'attack')
        hostileAction(pendingActor, mobs[i], 'attacks', random(1,8), 0);
      else if (pendingAction == 'firebolt')
        hostileAction(pendingActor, mobs[i], 'burns', random(5,10), 5);
      else if (pendingAction == 'lightning')
        hostileAction(pendingActor, mobs[i], 'zaps', random(0,15), 5);

      waitingforMobTarget=false;
      nextTurn();
    }

    //applies damage to a target
    var hostileAction = function(attacker, defender, verb, dmg, mp) {
      if (attacker.mana < mp) {
        //should never get here - but for testing purposes 'fizzle' on low mana
        status.message = 'not enough mana!';
        return;
      }
      //attack, zap or otherwise maim the target
      status.message = attacker.name + ' ' + verb + ' ' + defender.name + ' for ' + dmg + ' damage!';
      defender.health -= dmg;
      attacker.mana -= mp;
      if (defender.health <= 0) {
        defender.health = 0;
        defender.ko = true;
        status.message += ' Killing them!';
      }

    }

    //expose the guts of this factory class to the rest of the app in order
    //to tie together the program flow
    return {
      getCharacters : function () {
          return characters;
      },
      getCharacter : function (i) {
          return characters[i];
      },
      getMobs: function(){
          return mobs;
      },
      getMob: function(i){
          return mobs[i];
      },
      getActions: function(){
        return actions;
      },
      getStatus: function(){
        return status;
      },
      getHostileActionHandler: function(){
        return hostileActionHandler;
      }
    };
  }]);

  /*
    PLAYER STAT PANEL
      UI element that displays the name health and mana of
      a party member.
  */
  var playerStatPanelControllerMethod = ['$scope', 'battleFactory', function($scope, battleFactory) {
  }];

  app.directive('playerStatPanel', function() {
    return {
      restrict: 'E',
      scope: {
        character: '='
      },
      templateUrl: 'player-stat-panel.html',
      controller: playerStatPanelControllerMethod,
      controllerAs: 'statCtrl'
    };
  });

  /*
    PLAYER INPUT PANEL
      UI element that displays the actions a player can take.
      User can select an action to invoke it's functionality
  */
  var playerInputPanelControllerMethod = ['$scope', 'battleFactory', function($scope, battleFactory) {
    var actions = battleFactory.getActions();
    $scope.subMenu = false;
    $scope.currentActions = actions;
    $scope.actionDone = false;

    $scope.$watch('character.hasTurn', function(newVal, oldVal) {
        $scope.actionDone = false;
    }, true);

    $scope.handle = function(i) {
      if ($scope.currentActions[i].invoke != undefined) {
        $scope.currentActions[i].invoke($scope.character.index);
        $scope.currentActions = actions;
        $scope.actionDone = true;
        $scope.subMenu = false;
      }
      else if ($scope.currentActions[i].actions != undefined && $scope.currentActions[i].actions.length > 0) {
        $scope.currentActions = actions[i].actions;
        $scope.subMenu = true;
      }
    };

    $scope.back = function() {
      $scope.currentActions = actions;
      $scope.subMenu = false;
    };

  }];

  app.directive('playerInputPanel', function() {
    return {
      restrict: 'E',
      scope: {
        character: '='
      },
      templateUrl: 'player-input-panel.html',
      controller: playerInputPanelControllerMethod,
      controllerAs: 'inputCtrl'
    };
  });

  /*
    PLAYER PANEL
      Brings together all the UI elements to represent and
      control 1 player character on the field
  */
  var playerPanelControllerMethod = ['$scope', '$timeout', function($scope, $timeout) {
    $scope.damageTaken = 0;
    $scope.$watch('character.health', function(newVal, oldVal) {
        $scope.damageTaken = oldVal - newVal;
        $timeout(function(){$scope.damageTaken = 0;},1000);
    }, true);
  }];

  app.directive('playerPanel', function() {
    return {
      restrict: 'E',
      scope: {
        character: '='
      },
      templateUrl: 'player-panel.html',
      controller: playerPanelControllerMethod,
      controllerAs: 'playerCtrl'
    };
  });

  /*
    MOB PANEL
      Brings together all the UI elements to represent 1 enemy
  */
  var mobPanelControllerMethod = ['$scope', 'battleFactory', '$timeout', function($scope, battleFactory, $timeout) {
    $scope.handler = battleFactory.getHostileActionHandler();
    $scope.damageTaken = 0;

    $scope.$watch('mob.health', function(newVal, oldVal) {
        $scope.damageTaken = oldVal - newVal;
        $timeout(function(){$scope.damageTaken = 0;},1000);
    }, true);
  }];

  app.directive('mobPanel', function() {
    return {
      restrict: 'E',
      scope: {
        mob: '='
      },
      templateUrl: 'mob-panel.html',
      controller: mobPanelControllerMethod,
      controllerAs: 'mobCtrl'
    };
  });

  /*
    STATUS BAR
      displays status message
  */
  var statusBarControllerMethod = ['$scope', 'battleFactory', function($scope, battleFactory) {
    $scope.status = battleFactory.getStatus();
  }];

  app.directive('statusBar', function() {
    return {
      restrict: 'E',
      templateUrl: 'status-bar.html',
      controller: statusBarControllerMethod,
      controllerAs: 'statusCtrl'
    };
  });

})();
